package br.com.endoffile.flow.Exception;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConstraintViolationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

    @Inject
    private Logger LOG;
    
    @Override
    public Response toResponse(ConstraintViolationException exception) {

        List<GenericError> errors = exception.getConstraintViolations()
                .stream()
                .map(this::toValidationError)
                .collect(Collectors.toList());

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .encoding("UTF-8")
                .build();
    }

    private GenericError toValidationError(@SuppressWarnings("rawtypes") ConstraintViolation constraintViolation) {
    	GenericError genericError = new GenericError();
        constraintViolation.getPropertyPath().iterator()
        .forEachRemaining( p -> genericError.setPath(p.getName()) );
        genericError.setMessage(constraintViolation.getMessage());
        LOG.info(" [api.exception.validation] - "+ genericError);
        return genericError;
    }
}
