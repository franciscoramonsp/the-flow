package br.com.endoffile.flow.Exception;

import javax.inject.Inject;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class NotAllowMethodExceptionHandler implements ExceptionMapper<NotAllowedException> {

    @Inject
    private Logger LOG;

    @Context
    UriInfo uriInfo;

    @Override
    public Response toResponse(NotAllowedException exception) {

        return Response
                .status(Response.Status.METHOD_NOT_ALLOWED)
                .type(MediaType.APPLICATION_JSON)
                .encoding("UTF-8")
                .entity(toValidationError(exception)).build();
    }
    private GenericError toValidationError(NotAllowedException exception) {
        GenericError error = new GenericError();
        error.setPath(uriInfo.getBaseUri() + uriInfo.getPath());
        error.setMessage(exception.getMessage());

        LOG.log(Level.WARNING,"[ API.EXCEPTION ] - {0}", error.toString());

        return error;
    }
}
