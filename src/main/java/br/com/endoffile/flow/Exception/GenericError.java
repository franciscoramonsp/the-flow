package br.com.endoffile.flow.Exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenericError {
	
    private String path;
    private String message;
    
    
}
