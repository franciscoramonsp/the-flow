package br.com.endoffile.flow.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/v1/")
public class RestConfigApplication extends Application {
}
